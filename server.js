import app from "./app";

const config = require("./config");

const server = app.listen(config.port, () => {});

module.exports = server;
