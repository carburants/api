import express from "express";
import Stations from "../../middleware/Stations";
import { formatResponse } from "../../libs/Format";

const router = express.Router();

router.route("/").get(function(req, res, next) {
  Stations.getAll(req, (err, response) => {
    formatResponse(req, res, next, err, response);
  });
});

export default router;
