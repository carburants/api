module.exports = {
  parser: "babel-eslint",
  extends: ["airbnb-base", "prettier", "plugin:jest/recommended"],
  plugins: ["prettier", "jest"],
  env: {
    es6: true,
    browser: false,
    node: true,
    "jest/globals": true
  },
  globals: {
    __DEV__: true
  },
  rules: {
    // enable additional rules
    "no-plusplus": [2, { allowForLoopAfterthoughts: true }],
    "no-underscore-dangle": "off",
    indent: ["error", 2, { SwitchCase: 1 }],
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "double"],
    semi: ["error", "always"],
    // Forbid the use of extraneous packages
    // https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-extraneous-dependencies.md
    "import/no-extraneous-dependencies": ["error", { packageDir: "." }],
    // ESLint plugin for prettier formatting
    // https://github.com/prettier/eslint-plugin-prettier
    "prettier/prettier": "error",
    "func-names": ["error", "never"],
    "no-restricted-globals": ["error", "event", "fdescribe"]
  }
};