module.exports = {
  port: process.env.PORT || 3000,
  trustProxy: process.env.TRUST_PROXY || "loopback",
  mongodbUrl:
    process.env.DATABASE_URL || "mongodb://localhost:27017/prix-carburants",
  mongodbDebug: process.env.DEBUG || true,
  mondeDbOptions: {
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
};
