/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import fs from "fs";
import path from "path";
import Mongoose from "mongoose";
import config from "../config";

const basename = path.basename(__filename);

Mongoose.set("useNewUrlParser", true);
Mongoose.set("useFindAndModify", false);
Mongoose.set("useCreateIndex", true);
Mongoose.set("debug", config.mongodbDebug);

Mongoose.connect(config.mongodbUrl, config.mondeDbOptions);

const db = () => {
  const m = {};

  fs.readdirSync(__dirname)
    .filter(file => {
      return (
        file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
      );
    })
    .forEach(file => {
      const model = require(path.resolve(__dirname, file))(Mongoose);
      m[model.modelName] = model;
    });

  return m;
};

const models = db();

export const mongoose = Mongoose;
export default models;
