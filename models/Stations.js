/**
 * Model permettant de bufferiser les events reçus de Kafka
 */
module.exports = mongoose => {
  const schema = new mongoose.Schema({
    stationId: String,
    name: String,
    location: {
      type: { type: String },
      coordinates: []
    },
    prices: [
      {
        gasType: String,
        price: Number,
        updatedAt: Object
      }
    ],
    services: [],
    postCode: String,
    address: String,
    city: String
  });

  schema.index({ location: "2dsphere" });

  const Stations = mongoose.model("Stations", schema);

  Stations.createIndexes();

  return Stations;
};
